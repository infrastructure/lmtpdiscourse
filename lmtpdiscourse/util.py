from datetime import timedelta
import re

def tdelta(input):

    keys = ['weeks', 'days', 'hours', 'minutes', 'seconds', 'milliseconds']
    regex = ''.join(['((?P<%s>\d+)%s ?)?' % (k, k[0]) for k in keys])
    kwargs = {}
    for k,v in re.match(regex, input).groupdict(default='0').items():
        kwargs[k] = int(v)
    return timedelta(**kwargs)
