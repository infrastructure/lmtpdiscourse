class ConnectionClosed(Exception):
    pass

class ConnectionLost(ConnectionClosed):
    pass

class ConnectionTimeout(ConnectionClosed):
    pass

class ServiceInShutdown(ConnectionClosed):
    pass

class SocketNotAvailable(Exception):
    pass
