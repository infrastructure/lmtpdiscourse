from grabbox.logbook import TaggingLogger

lmtpdiscourse = TaggingLogger('lmtpdiscourse', tag='lmtpdiscourse')

debug = lmtpdiscourse.debug
info = lmtpdiscourse.info
notice = lmtpdiscourse.notice
warn = lmtpdiscourse.warn
warning = lmtpdiscourse.warning
error = lmtpdiscourse.error
critical = lmtpdiscourse.critical
exception = lmtpdiscourse.exception
log = lmtpdiscourse.log
