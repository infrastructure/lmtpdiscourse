import asyncio
from contextlib import closing
# from concurrent.futures import ProcessPoolExecutor

import aiohttp
import lya
import click
# from mailer import Mailer

from grabbox.click.logbook import init_logbook, LogHandler, LogLevel
from grabbox.logbook import catch_exceptions as _catch_exceptions

from . import logging as log
from .server import Server


def setup_shutdown_signal_handlers(loop,signals):
    import signal
    log.info('Shutting down on signals {}',signals)
    for signame in signals:
        def initiate_shutdown_for_signal():
            log.notice('Shutting down due to signal {}',signame)
            loop.stop()
        loop.add_signal_handler(getattr(signal,signame),initiate_shutdown_for_signal)


def catch_exceptions():
    return _catch_exceptions(
        logger = log,
        ignore_exceptions = (
            KeyboardInterrupt,
            click.ClickException,
            click.Abort,
            ),
        )


@click.command()
@init_logbook(
    logger = log,
    catch_exceptions = catch_exceptions,
    default_level = LogLevel.INFO,
)
@click.option(
    '--config','-c',
    type=click.Path(exists=True,resolve_path=True,readable=True),
    default='/etc/lmtpdiscourse.yaml',
    help='configuration file (/etc/lmtpdiscourse.yaml)')
def lmtpdiscourse(config):

    config = lya.AttrDict.from_yaml(config)

    loop = asyncio.get_event_loop()
    with closing(loop):

        setup_shutdown_signal_handlers(loop,('SIGINT','SIGTERM'))

        session = aiohttp.ClientSession(loop=loop)
        with closing(session):
            srv = Server(
                loop=loop,
                session=session,
                config=config,
                )
            try:
                r = loop.create_task(srv.start_server())
                loop.run_until_complete(r)
                r.result()
                loop.run_forever()
            finally:
                if srv.running:
                    r = loop.create_task(srv.close())
                    loop.run_until_complete(r)
                    r.result()
