import asyncio
from pathlib import Path
from urllib.parse import urljoin
import arrow
from contextlib import closing
import os
import shutil
import pwd
import grp
import re
from io import StringIO
from pathlib import Path
import uuid

from . import logging as log
from .exceptions import ServiceInShutdown, ConnectionLost, ConnectionTimeout, ConnectionClosed, SocketNotAvailable
from .util import tdelta


class Server:

    LHLO = re.compile('LHLO (\\S+)\r?\n',re.IGNORECASE)
    MAIL = re.compile('MAIL FROM:\\s*(.+>)\\s*([^\r]*)\r?\n',re.IGNORECASE)
    RCPT = re.compile('RCPT TO:\\s*(.+>)\\s*\r?\n',re.IGNORECASE)
    address = re.compile('.*<(.*)>.*')
    msg_end = re.compile('^\\.\r?\n')

    def __init__(self,loop,session,config):

        self.loop = loop
        self.session = session
        self.fetch_task = None
        self.running = False
        self.closing = False
        self.config = config
        self.site = config.discourse.url
        self.api_user = config.discourse.api_user
        self.api_key = config.discourse.api_key
        self.fqdn = config.server.fqdn
        self.active_connections = {}

        self.poll_step = tdelta(config.server.poll_step).total_seconds()
        self.poll_interval = tdelta(config.server.poll_interval).total_seconds()

        self.read_timeout = tdelta(config.server.timeout).total_seconds()
        self.read_timeout_step = tdelta(config.server.read_step).total_seconds()

        self.email_url = urljoin(self.site,'admin/email/handle_mail')
        self.categories_url = urljoin(self.site,'categories.json')
        self.category_url = urljoin(self.site,'c/{}/show.json')
        self.settings_url = urljoin(self.site,'admin/site_settings.json')

        self.poll_settings_interval = tdelta(config.server.poll_settings_interval).total_seconds()
        self.first_settings_poll = True

        log.info('Poll interval for Discourse settings: {} s',self.poll_settings_interval)

        self.have_valid_recipients = False
        self.valid_recipients = []
        self.empty_valid_recipients_regex = re.compile(b'^$')
        self.valid_recipients_regex = self.empty_valid_recipients_regex


    @asyncio.coroutine
    def start_server(self):

        socket = Path(self.config.server.unix_socket.path)

        if socket.exists():
            if socket.is_socket():
                log.notice('Removing existing socket at {}',str(socket))
                socket.unlink()
            else:
                raise SocketNotAvailable('File at {} is not a socket, refusing to delete'.format(str(socket)))

        self.server = yield from asyncio.start_unix_server(self.handle_connection,path=str(socket),loop=self.loop)

        if os.getuid() == 0:
            log.info('Setting privileges of unix socket to {:o}',self.config.server.unix_socket.mode)
            socket.chmod(self.config.server.unix_socket.mode)
            log.info('Changing ownership of unix socket to {}:{}',self.config.server.unix_socket.owner,self.config.server.unix_socket.group)

            socket_uid = pwd.getpwnam(self.config.server.unix_socket.owner).pw_uid
            socket_gid = grp.getgrnam(self.config.server.unix_socket.group).gr_gid

            os.chown(str(socket),socket_uid,socket_gid)

            os.setgroups([])

            uid = pwd.getpwnam(self.config.server.user).pw_uid
            gid = grp.getgrnam(self.config.server.group).gr_gid

            log.notice('Dropping root privileges, switching to {} (uid={}):{} (gid={})',self.config.server.user,uid,self.config.server.group,gid)

            os.setgid(gid)
            os.setuid(uid)

            if os.getgid() != gid or os.getuid() != uid:
                raise os.error('Could not drop privileges')

            log.debug('Privileges dropped successfully')

        self.running = True
        self.fetch_valid_recipients()

        log.notice('LMTP to Discourse gateway started at {config.server.unix_socket.path}',config=self.config)
        log.notice('Talking to Discourse at {config.discourse.url}',config=self.config)

    @asyncio.coroutine
    def close(self):
        if not self.closing:
            self.server.close()
        self.closing = True
        yield from asyncio.gather(self.fetch_task,self.server.wait_closed(),*self.active_connections.values(),loop=self.loop)


    @asyncio.coroutine
    def sleep(self,timeout=None):
        if timeout is None:
            timeout = self.poll_interval
        step = min(timeout,self.poll_step)
        waited = 0
        while waited < timeout and not self.closing:
            yield from asyncio.sleep(step,loop=self.loop)
            waited += step

    def encode(self,string):
        return string.encode('utf-8')

    def decode(self,bytes_):
        return bytes_.decode('utf-8')

    # this wrapper just makes sure to inject a connection id and to register the connection for cleanup during shutdown
    def handle_connection(self,reader,writer):
        connection_id = uuid.uuid4()
        handler = self.loop.create_task(self._handle_connection(reader,writer,connection_id))
        self.active_connections[connection_id] = handler
        return handler

    @asyncio.coroutine
    def _handle_connection(self,reader,writer,connection_id):
        try:
            with closing(writer):

                peer_msg = ''

                def write(data):
                    writer.write(self.encode(data))

                def check_closing():
                    if self.closing:
                        log.info('Closing connection{} due to server shutdown',peer_msg)
                        write('221 {} closing connection (server shutdown)\r\n'.format(self.fqdn))
                        raise ServiceInShutdown()

                def readline():
                    wait_time = 0.0
                    while True:
                        check_closing()
                        try:
                            data = yield from asyncio.wait_for(reader.readline(),self.read_timeout_step,loop=self.loop)
                            break
                        except asyncio.TimeoutError:
                            wait_time += self.read_timeout_step
                            if wait_time >= self.read_timeout:
                                log.info('Closing connection{} due to client inactivity',peer_msg)
                                write('221 {} closing connection (session timeout)\r\n'.format(self.fqdn))
                                raise ConnectionTimeout()

                    if len(data) == 0:
                        log.info('Connection closed by client')
                        raise ConnectionLost()
                    return self.decode(data)

                def readbody():
                    buf = StringIO()
                    line = yield from readline()
                    buf.write(line)
                    while not self.msg_end.match(line):
                        line = yield from readline()
                        buf.write(line)
                    return buf.getvalue()


                while not self.have_valid_recipients:
                    log.info('Not responding to request until valid recipients retrieved, waiting...')
                    yield from self.sleep()

                write('220 {} LMTP Discourse gateway ready\r\n'.format(self.fqdn))
                req = yield from readline()
                log.debug('Got request: {}',req.rstrip('\r\n'))
                m = self.LHLO.match(req)
                if not m:
                    write('502 5.5.2 Unknown command\r\n')
                    write('221 {} closing connection\r\n'.format(self.fqdn))
                    return
                peer = m.group(1)
                peer_msg = ' with {}'.format(peer)
                log.info('New connection from {}',peer)

                write('250-{}\r\n'.format(self.fqdn))
                write('250-8BITMIME\r\n')
                write('250-ENHANCEDSTATUSCODES\r\n')
                write('250 PIPELINING\r\n')

                recipient = None
                data = None
                sender = None

                while True:

                    req = yield from readline()
                    log.debug('Got request: {}',req.rstrip('\r\n'))

                    cmd = req[:4].upper()

                    if cmd == 'MAIL':
                        m = self.MAIL.match(req)
                        if not m:
                            write('501 5.5.4 Invalid parameters\r\n')
                            continue
                        if len(m.group(2)) > 0:
                            log.debug('Got additional sender info: {}',m.group(2))
                        if sender:
                            write('503 5.5.1 MAIL already given\r\n')
                            continue
                        sender = m.group(1)
                        write('250 2.1.0 OK\r\n')
                        log.debug('Starting new mail, sender: {}',sender)

                    elif cmd == 'RCPT':
                        m = self.RCPT.match(req)
                        if not m:
                            write(b'501 5.5.4 Invalid parameters\r\n')
                            continue

                        r = self.address.match(m.group(1))
                        if not r:
                            write(b'501 5.1.0 {} Invalid recipient address syntax\r\n'.format(m.group(1)))
                            continue


                        if recipient:
                            write('451 4.3.0 {} Cannot post the same message to multiple conversations\r\n'.format(m.group(1)))
                            log.debug('Client attempted to set multiple target addresses, old: {}, new: {}',recipient,m.group(1))
                            continue

                        recipient = r.group(1)

                        if not (self.valid_recipients_regex.match(recipient) or recipient in self.valid_recipients):
                            log.debug('Invalid recipient: {}',recipient)
                            recipient = None
                            write('550 5.5.1 {} No such incoming email address in Discourse\r\n'.format(recipient))
                            continue

                        write('250 2.1.5 OK\r\n')
                        log.debug('Got recipient: {}',sender)


                    elif cmd == 'DATA':

                        if req.strip().upper() != 'DATA':
                            write('501 5.5.4 Invalid parameters\r\n')
                            continue

                        if sender is None:
                            write('503 5.5.1 MAIL needed first\r\n')
                            continue

                        if recipient is None:
                            write('554 5.5.1 No valid recipients\r\n')
                            continue

                        write('354 Start mail input; end with <CRLF>.<CRLF>\r\n')
                        log.debug('Receiving mail from {} for {}',sender,recipient)

                        data = yield from readbody()

                        log.debug('message contents:\n{}',data)
                        response = yield from self.session.post(self.email_url,data={'api_username': self.api_user, 'api_key': self.api_key, 'email': data})

                        ok = response.status == 200
                        if not ok:
                            reason = yield from response.text()

                        yield from response.release()

                        if ok:
                            write('250 2.0.0 {} relayed to Discourse\r\n'.format(recipient))
                            log.info('Relayed message from {} to {}',sender,recipient)
                        else:
                            write('451 4.3.0 {} Internal error relaying message, see server logs\r\n')
                            log.error('Failed to relay message from {} to {}, Discourse response: {}',sender,recipient,reason)

                        recipient = None
                        data = None
                        sender = None

                    elif cmd == 'RSET':

                        if req.strip().upper() != 'RSET':
                            write('501 5.5.4 Invalid parameters\r\n')
                            continue

                        log.debug('Resetting connection{}',peer_msg)
                        recipient = None
                        data = None
                        sender = None

                    elif cmd == 'QUIT':

                        if req.strip().upper() != 'QUIT':
                            write('501 5.5.4 Invalid parameters\r\n')
                            continue

                        write('221 {} closing connection\r\n'.format(self.fqdn))
                        log.info('Closing connection{}',peer_msg)
                        return

                    else:
                        write('502 5.5.2 Unknown command\r\n')
                        continue

        except ConnectionClosed:
            pass
        except Exception as e:
            log.exception('Error handling connection: {}',e)
        finally:
            # unregister connection
            del self.active_connections[connection_id]


    def fetch_valid_recipients(self):
        if not self.running:
            raise ServiceNotRunning('Cannot start fetch_valid_recipients tasks on a server that is not running')
        if self.closing:
            raise ServiceInShutdown('Cannot start fetch_valid_recipients tasks on a server that is closing')
        log.notice('Starting poll for valid recipient email addresses')
        self.fetch_task = (self.loop.create_task(self._fetch_valid_recipients()))


    @asyncio.coroutine
    def _fetch_valid_recipients(self):

        while True:

            if not self.first_settings_poll:
                yield from self.sleep(self.poll_settings_interval)
            self.first_settings_poll = False

            if self.closing:
                return

            log.info('Updating valid recipients from Discourse')

            response = yield from self.session.get(self.categories_url,params={'api_key' : self.api_key, 'api_username' : self.api_user})

            if response.status != 200:
                log.error('Error retrieving categories: {}',(yield from response.text()))
                continue

            categories = (yield from response.json())['category_list']['categories']

            all_categories = set()
            for c in categories:
                all_categories.add(c['id'])
                try:
                    all_categories.update(c['subcategory_ids'])
                except KeyError:
                    pass


            valid_recipients = set()

            for c in all_categories:
                response = yield from self.session.get(self.category_url.format(c),params={'api_key' : self.api_key, 'api_username' : self.api_user})

                if response.status != 200:
                    log.error('Error retrieving category: {}',(yield from response.text()))
                    continue

                email = (yield from response.json())['category']['email_in']
                if email:
                    valid_recipients.add(email)

            self.valid_recipients = valid_recipients
            log.debug('Valid recipients: {}',valid_recipients)

            response = yield from self.session.get(self.settings_url,params={'api_key' : self.api_key, 'api_username' : self.api_user})

            if response.status != 200:
                log.error('Error retrieving Discourse settings: {}',(yield from response.text()))
                continue

            settings = yield from response.json()

            found = 0
            patterns = []
            for setting in settings['site_settings']:

                if setting['setting'] == 'reply_by_email_address':
                    found += 1
                    if len(setting['value']) > 0:
                        patterns.append(setting['value'])
                elif setting['setting'] == 'alternative_reply_by_email_addresses':
                    found += 1
                    patterns.extend(list(p for p in setting['value'].split('|') if len(p) > 0))

            regex = '|'.join(re.escape(p.replace('%{reply_key}','REPATTERNMATCH')).replace('REPATTERNMATCH','[^ @]+') for p in patterns)

            valid_recipients_regex = re.compile(regex)

            if len(regex) > 0:
                self.valid_recipients_regex = valid_recipients_regex
            else:
                self.valid_recipients_regex = self.empty_valid_recipients_regex

            log.debug('Valid recipients regex: {}',regex)

            self.have_valid_recipients = True
