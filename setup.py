from setuptools import setup, find_packages

setup(
    name = 'lmtpdiscourse',

    version = '1.0.0',

    description  = 'Simple LMTP server feeding emails into Discourse',

    url = 'http://gitlab.dune-project.org/infrastructure/lmtpdiscourse',

    author = 'Steffen Müthing',
    author_email = 'steffen.muething@iwr.uni-heidelberg.de',

    license='BSD',

    packages=find_packages(),

    install_requires = [
        'aiohttp',
        'arrow',
        'click',
        'layered-yaml-attrdict-config',
        'logbook',
        'grabbox==0.1.0',
        ],

    dependency_links = [
        'git+https://parcomp-git.iwr.uni-heidelberg.de/smuething/grabbox.git#egg=grabbox-0.1.0',
        ],

    entry_points={
        'console_scripts' : [
            'lmtpdiscourse=lmtpdiscourse:lmtpdiscourse',
            ],
        },
)
